import './App.css';
import { Container, Row, Navbar, Nav, NavDropdown, Carousel } from 'react-bootstrap';
import Main from './screens/route';
import { Link } from 'react-router-dom';


function App() {
  return (
      <div style={{backgroundColor:"#efefef", height:'100%'}}>
        <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark" sticky="top">
          <Link to="/">
              <Navbar.Brand>React-Bootstrap</Navbar.Brand>
          </Link>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="mr-auto">
            </Nav>
            <Nav>
              <Link to="/resume">
                <Nav.Link href="/">Resume</Nav.Link>
              </Link>
              <Link to="/aboutme">
                <Nav.Link href="/">Aboutme</Nav.Link>
              </Link>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
        <Container fluid>
            <Main/>
        </Container>
    </div>
  );
}

export default App;
