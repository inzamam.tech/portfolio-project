import React, { Component } from 'react'
import { Row, Carousel } from 'react-bootstrap';
import Blinking from '../../assets/blinking.JPG';
import Chatapp from '../../assets/chatapp.JPG';
import Covid from '../../assets/COVID.jpg';

class Home extends Component {
  render() {
    return (
    <Row>
          <Carousel style={{ height:500 }}>
            <Carousel.Item>
                <img
                style={{height:500, width:'100%'}}
                className="d-block w-100"
                src={Blinking}
                alt="First slide"
                />
                <Carousel.Caption>
                <h3>First slide label</h3>
                <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
                </Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item>
                <img
                style={{height:500, width:'100%'}}
                className="d-block w-100"
                src={Chatapp}
                alt="Third slide"
                />
                <Carousel.Caption>
                <h3>Second slide label</h3>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                </Carousel.Caption>
            </Carousel.Item>
        </Carousel>
    </Row>
    )
  }
}


export default Home