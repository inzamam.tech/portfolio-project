import React, { Component } from 'react'
import {Row, Col, Image } from 'react-bootstrap';
import Chatapp from '../../assets/chatapp.JPG';


class Resume extends Component {
  render() {
    return (
        <Row className="justify-content-md-center" style={{ height: "100%" }}>
            <Col xs lg="2" style={{ height:600 ,backgroundColor:'white', marginTop:80, marginRight:15, borderRadius:20 }}>
                <Row style={{ justifyContent:'center', alignItems:'center'}}>
                    <Image src={Chatapp} roundedCircle style={{ height:150, width:150 , marginTop:20}}/>
                </Row>
            </Col>
            <Col xs lg="5" style={{ height:600, backgroundColor:'yellowgreen', marginTop:80, marginLeft:15, borderRadius:20 }}>
            </Col>
        </Row>
    )
  }
}


export default Resume