import React from "react";
import { Switch, Route } from "react-router-dom";
import Home from './home';
import Aboutme from './about';
import Resume from './resume';
import { Redirect } from "react-router-dom/cjs/react-router-dom.min";

const Main = () => (
    <Switch>
      <Route exact path="/" component={Home} />
      <Route path="/resume" component={Resume} />
      <Route path="/aboutme" component={Aboutme} />
      <Route>
          <Redirect to="/"/>
      </Route>
    </Switch>
  );

  export default Main;  